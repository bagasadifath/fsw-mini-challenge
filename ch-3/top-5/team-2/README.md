### Deskripsi challenge
- Buatlah agar tampilan mobile & tablet berbeda dengan tampilan desktop
- Lebar mobile & tablet adalah <= 768px

## Tampilan Desktop
![image.png](./image.png)

## Tampilan Mobile & Tablet
![image-1.png](./image-1.png)


